/*

Laser.hx

Description

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import flash.display.Sprite;
import flash.display.Bitmap;

import fi.zuigeproductions.ld48.SpriteSheet;

class Laser extends Sprite {
	
	public function new () {
		
		super();
		
		addChild(new Bitmap(SpriteSheet.loadSprite("LASER_SRC")));
		
	}	
	
	private function oscillate():Void {
	
		
	
	}	

}
