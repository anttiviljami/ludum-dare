/*

Robot.hx

Description

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import flash.display.Sprite;
import flash.display.Bitmap;
import flash.display.BitmapData;

import flash.geom.Point;

import fi.zuigeproductions.ld48.SpriteSheet;

class Robot extends Sprite {

	public var bitmap:Bitmap;
	
	public var rot:Float;
	public var spd:Float;
	
	public var acc:Float;
	public var tor:Float;
	
	public  var maxspd:Float = 0;
	
	public static var maxrot:Float = 2.5;
	
	public var cycles:Int;
	
	public var pos:Point;
	public var ltclaw:Point;
	public var rtclaw:Point;
	public var back:Point;
	//public var 
	
	public function new () {
		
		super();
		
		acc = spd = rot = tor = 0;
				
		bitmap = new Bitmap();
		bitmap.bitmapData = SpriteSheet.loadSprite("GIMP");
	
		
		addChild(bitmap);
		bitmap.x = -bitmap.width / 2;
		bitmap.y = -bitmap.height / 2;
		
		
	}
	
	public function move () {
		
		if(Math.abs(acc) > 0 || Math.abs(tor) > 0)
			cycles -= Math.floor( 1000/60 );
		
		else {
		
			//friction
			acc = .8*spd - spd;
			tor = .8*rot - rot;
		
		}
		
		if( Math.abs(tor)/tor + Math.abs(rot)/rot == 0) {
			rot = 0;
		}
		
		spd += acc;
		rot += tor;
		
		if(spd > maxspd) 
			spd = maxspd * Math.abs(spd) / spd;
			
		if(spd < -maxspd * .75) 
			spd = .75 * maxspd * Math.abs(spd) / spd;
			
		if(Math.abs(rot) > maxrot) 
			rot = maxrot * Math.abs(rot) / rot;
			
		//move robot		
		
		if(Math.abs(spd) >= .01) {
			x += spd * Math.cos(Math.PI * rotation / 180);
			y += spd * Math.sin(Math.PI * rotation / 180);
		} 
		
		else spd = 0;
		
		rotation += rot;
		
	}

}
