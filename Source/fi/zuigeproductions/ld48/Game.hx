/*

Game.hx

Description

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import flash.Lib;
import flash.display.Sprite;
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.MouseEvent;

import flash.ui.Keyboard;

import flash.display.Graphics;
import flash.display.GraphicsGradientFill;
import flash.display.GradientType;
import flash.display.SpreadMethod;

import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.Font;
import flash.text.AntiAliasType;

import flash.geom.Matrix;
import flash.geom.Point;
import flash.geom.Rectangle;

import flash.media.Sound;

import flash.display.DisplayObject;

import flash.system.System;

import openfl.Assets;

import fi.zuigeproductions.ld48.Main;
import fi.zuigeproductions.ld48.Robot;
import fi.zuigeproductions.ld48.SpriteSheet;
import fi.zuigeproductions.ld48.Overlay;
import fi.zuigeproductions.ld48.Map;
import fi.zuigeproductions.ld48.Git;
import fi.zuigeproductions.ld48.CoverScreen;

class Game extends Sprite {

	public var owner:Main;

	public var ticks:Int = 0;
	public var robot:Robot;
	public var git:Git;
	
	public var lvl:Int = 1;
	
	public var cassettes:Int = 5;
	
	public var running:Bool = false;
	public var paused:Bool = false;
	
	public var fullscreen:Bool = false;

	private var bg:Sprite;
	private var cam:Sprite;
	private var hud:Overlay;
	
	private var world:Sprite;
	private var map:Map;
	private var animations:Sprite;
	
	private var camPos:Point;
	private var camVel:Point;
	
	private var camRect:Rectangle;
	private var renderRect:Rectangle;
	
	private var pauseMenu:Sprite;
	private var cover:CoverScreen;
	
	private var flasher:Sprite;
	private var animation:Bitmap;
	
	private var wait:Int;
	
	private var soundbtnContainer:Sprite;
	private var soundbtn:Bitmap;
	
	private var exitbtnContainer:Sprite;
	private var exitbtn:Bitmap;
	
	private var lastCheckPoint:Int = 1;
	
	private var cassette:Bitmap;
	
	private var functionFadeout:Dynamic;
	
	private var cheatChars:Array<Int>;
	
	public function new (owner) {
	
		super();
		
		this.owner = owner;
	}		
	
	public function startGame():Void {
		
		playSound("Assets/Pickup_Coin80.wav");
		
		running = true;
		
		cheatChars = new Array();
		
		Lib.current.stage.addEventListener(Event.ENTER_FRAME, on_enterFrame);
		Lib.current.stage.addEventListener(KeyboardEvent.KEY_DOWN, on_keyDown);
		Lib.current.stage.addEventListener(KeyboardEvent.KEY_UP, on_keyUp);
		
		bg = new Sprite();
		cam = new Sprite();
		map = new Map("Assets/level0.png");
		hud = new Overlay(this);
		
		animations = new Sprite();
		
		pauseMenu = new Sprite();
		pauseMenu.addChild(new Bitmap(SpriteSheet.loadSprite("PAUSEMENU")));
		pauseMenu.visible = false;
		
		soundbtnContainer = new Sprite();
		soundbtn = new Bitmap();
		soundbtn.bitmapData = SpriteSheet.loadSprite("SOUND_ON");
		
		soundbtnContainer.addChild(soundbtn);
		soundbtnContainer.x = Main.GAME_WIDTH - 40;
		soundbtnContainer.y = 8;
		
		pauseMenu.addChild(soundbtnContainer);
		
		soundbtnContainer.addEventListener(MouseEvent.CLICK, toggleSound);
		
		#if !flash
		
		exitbtnContainer = new Sprite();
		exitbtn = new Bitmap();
		exitbtn.bitmapData = SpriteSheet.loadSprite("EXIT");
		
		exitbtnContainer.addChild(exitbtn);
		exitbtnContainer.x = 8;
		exitbtnContainer.y = 8;
		
		pauseMenu.addChild(exitbtnContainer);

		exitbtnContainer.addEventListener(MouseEvent.CLICK, exit);

		#end
						
		hud.addChild(pauseMenu);
		
		cover = new CoverScreen();
		cover.hide();
		hud.addChild(cover);
		
		cam.x = Main.GAME_WIDTH / 2;
		cam.y = Main.GAME_HEIGHT / 2;
	
		world = new Sprite();
		robot = new Robot();
		git = new Git();
		cassette = new Bitmap(SpriteSheet.loadSprite("CASSETTE"));
		cassette.visible = false;
		
		cam.addChild(world);
		
		newGame();
		
		showPrologue();
		
		owner.menu.visible = false;
		
	}
	
	private function tick():Void {
	
		++ticks;
				
		if(!running || paused) return; //do nothing		
		
		robot.acc = 0;
		robot.tor = 0;
		
		if(hud.buttons[5].state && robot.cycles > 0) {
		
			//play
			robot.acc = .06;	
			robot.maxspd = 1.75;
			
		}
		
		if(hud.buttons[0].state && robot.cycles > 0) {
		
			//rewind
			robot.maxspd = 3.5;
			robot.acc = -.09;
		}
				
		if(hud.buttons[1].state && robot.cycles > 0) { 
			
			//forward
			robot.maxspd = 3.5;
			robot.acc = .09;
		}
		
		if(hud.buttons[2].state && robot.cycles > 0) robot.tor = -.12; //ccw
		if(hud.buttons[3].state && robot.cycles > 0) robot.tor = .12; //cw
		
		if(robot.cycles < 0) 
			robot.cycles = 0;
	
		
		collisions();			
		
		robot.move();
		
		if(robot.cycles == 0 && robot.spd == 0) fail();
		
		git.rotation = Math.atan2(robot.y - git.y, robot.x - git.x) * 180 / Math.PI;
		
		hud.update();
				
		camVel.x = .04 * (robot.x + 50 * robot.spd * Math.cos(robot.rotation * Math.PI / 180) - camPos.x);
		camVel.y = .04 * (robot.y + 30 * robot.spd * Math.sin(robot.rotation * Math.PI / 180) - camPos.y);
		
		camPos = camPos.add(camVel);
		
		//render frame
		world.x = -camPos.x;
		world.y = -camPos.y;
		
		camRect = new Rectangle(
			camPos.x - Main.GAME_WIDTH / 2, 
			camPos.y - Main.GAME_WIDTH / 2,  
			Main.GAME_WIDTH,  
			Main.GAME_HEIGHT
		);
		
		//a slightly larger rectangle for safe rendering
		renderRect = camRect;
		renderRect.x -= 3*32;
		renderRect.y -= 3*32;
		renderRect.width += 2 * 3*32;
		renderRect.height += 2 * 3*32;
		
		map.renderRect(renderRect, Math.floor((lvl - 1) / 5));
		
		var index:Int = world.getChildIndex(animations);
		world.removeChildAt(index);
		animations = new Sprite();
		world.addChildAt(animations, index);
		
		for(obj in map.animated) {
		
			if(obj.type == BlockType.LASER)
			
				if(!renderRect.containsPoint(new Point(obj.pos.x * 32, obj.pos.y *32))) continue; //not in frame
				
				
				if(Math.floor(ticks / 60) % 2 == 0) {
				
					var sprite = new Bitmap(SpriteSheet.loadSprite("LASER"));
					sprite.x = obj.pos.x * 32;
					sprite.y = obj.pos.y * 32;
					
					animations.addChild(sprite);
					
				
				}
		
		}
		
	}
	
	private function collisions():Void {
		
		robot.pos = new Point(robot.x, robot.y);
		
		robot.back = new Point(
			robot.x - 6 * Math.cos(Math.PI * robot.rotation / 180),
			robot.y - 6 * Math.sin(Math.PI * robot.rotation / 180)
		);
		
		robot.rtclaw = new Point(
			robot.x + 15 * Math.cos(Math.PI * (robot.rotation + 45 ) / 180),
			robot.y + 15 * Math.sin(Math.PI * (robot.rotation + 45 ) / 180)
		);
		
		robot.ltclaw = new Point(
			robot.x + 15 * Math.cos(Math.PI * (robot.rotation - 45 ) / 180),
			robot.y + 15 * Math.sin(Math.PI * (robot.rotation - 45 ) / 180)
		);
		
		if((map.getBlock(asCoord(robot.back)).type == BlockType.WALL) 
		|| (map.getBlock(asCoord(robot.rtclaw)).type == BlockType.WALL)
		|| (map.getBlock(asCoord(robot.ltclaw)).type == BlockType.WALL)) {
			
			fail();
			
			return;
			
		}
		
		if((map.getBlock(asCoord(robot.pos)).type == BlockType.LASER) 
		&& Math.floor(ticks/60) % 2 == 0) {
			
			fail();
			
			return;
			
		}
		
		if((map.getBlock(asCoord(robot.back)).type == BlockType.GIT) 
		|| (map.getBlock(asCoord(robot.rtclaw)).type == BlockType.GIT)
		|| (map.getBlock(asCoord(robot.ltclaw)).type == BlockType.GIT)) {
			
			
			if(lvl == 15) {
			//if(Assets.getBitmapData("Assets/level"+ (lvl + 1) + ".png") == null) {
				playSound("Assets/Powerup52.wav");
				congratulate();
				return;
			}
			
			else {
	
				nextLevel();
				return;
			}
				
		}
		
	}
	
	private function newGame():Void {
		
		world.addChild(map);
		world.addChild(animations);
		world.addChild(robot);
		world.addChild(git);
		world.addChild(cassette);

		drawBG();
		
		robot.x = map.spawn.x*32 + 16;
		robot.y = map.spawn.y*32 + 16;
		
		if(lvl % 5 == 0) { 
			
			if(lvl == 15)
				cassette.bitmapData = SpriteSheet.loadSprite("TROPHY");
			else
				cassette.bitmapData = SpriteSheet.loadSprite("CASSETTE");
				
			git.visible = false;
			cassette.visible = true;
			
			cassette.x = map.git.x*32;
			cassette.y = map.git.y*32;
		}
		
		else {
			
			git.visible = true;
			cassette.visible = false;
		
			git.x = map.git.x*32 + 16;
			git.y = map.git.y*32 + 16;
			
		}
		
		for(obj in map.gameObjects) {
		
			if(obj.type == BlockType.LASER) {
				
				var laser:Bitmap = new Bitmap();
				
				laser.bitmapData = SpriteSheet.loadSprite("LASER_SRC");
				laser.x = obj.pos.x * 32;
				laser.y = obj.pos.y * 32;
				
				world.addChild(laser);
			
			}
		
		}
		
		camPos = new Point(robot.x, robot.y);
		camVel = new Point(0, 0);
		
		addChild(bg);
		addChild(cam);
		addChild(hud);
		
		for(btn in hud.buttons) {
			if(btn.state) btn.toggle();
		} 
		
		robot.cycles = 10000; //10s
		
		//initial tick
		tick();

	}
	
	private function reset():Void {
		
		if(!running) return;
		
		cam.removeChild(world);
		
		world = new Sprite();
		cam.addChild(world);
		
		map = new Map("Assets/level" + lvl + ".png");
		robot = new Robot();
		
		newGame();
		
	}
	
	private function fail():Void {
			
		playSound("Assets/Explosion48.wav");
		
		explode(robot.x, robot.y);
		
		robot.visible = false;
		
		running = false;
		
	}
	
	private function explosionCallback():Void {
	
		running = true;
	
		if(--cassettes == 0) 
			gameOver();
			
		else reset();
	
	}
	
	private function nextLevel():Void {
		
		if(lvl % 5 == 0) {
		
			playSound("Assets/Powerup52.wav");
			cassettes += 3;
			lastCheckPoint = lvl + 1;
		}
			
		else playSound("Assets/Powerup10.wav");
		
		++lvl;
		reset();
		
	}
	
	private function gameOver():Void {
		
		lvl = lastCheckPoint;
		
		cassettes = 5;
		
		reset();
		
		//cover.setGraphic("GAMEOVER");
		//cover.fadeIn();
		
		//running = false;
		
		//wait = ticks;
		
		//Lib.current.stage.addEventListener(MouseEvent.CLICK, restart);
	
	}
	
	private function congratulate():Void {
	
		cover.setGraphic("ENDSCREEN", "REPLAY");
		cover.fadeIn();
		running = false;
		
		lastCheckPoint = 1;
		
		wait = ticks;
		
		Lib.current.stage.addEventListener(MouseEvent.CLICK, restart);
	
	}
	
		
	private function showPrologue():Void {
	
		cover.setGraphic("PROLOGUE", "START GAME");
		cover.show();
		
		running = false;
		
		wait = ticks;
		
		Lib.current.stage.addEventListener(MouseEvent.CLICK, restart);
	
	}
	
	private function drawBG():Void {
	
		var variation:Int = Math.floor((lvl - 1)/ 5);
		var gradient:Array<Int> = new Array();
	
		if(variation == 1) 
			gradient = [0x7c838f, 0x5e656f];
		if(variation == 0) 
			gradient = [0x8b8478, 0xbbb19f];
	
		var gradientMatrix:Matrix = new Matrix();
		gradientMatrix.createGradientBox( Main.GAME_WIDTH, Main.GAME_HEIGHT, Math.PI/2, 0, 0 );
	
	    bg.graphics.beginGradientFill(GradientType.LINEAR, gradient, [1, 1], [0, 0xff], gradientMatrix, SpreadMethod.PAD );
	   	bg.graphics.drawRect(0, 0, Main.GAME_WIDTH, Main.GAME_HEIGHT);
	   	bg.graphics.endFill();
	}
	
	private function on_enterFrame(e:Event):Void {
		tick();
	}
	
	private function on_keyDown(e:KeyboardEvent):Void {
	//	trace(e.keyCode);
		
		if(e.keyCode == 13) {
			if(running && !paused) { 
				fail();
			}
		}
		
		if(e.keyCode == 32) {
			if(running && !paused) {
				for(btn in hud.buttons) 
					if(btn.state) btn.toggle();
					
				hud.buttons[4].toggle();
			
			}
		}
		
		if(e.keyCode == 27 || e.keyCode == 80) {
		
			togglePause();
		
		}
		
		if(paused) {
			var i:Int = 11;
			
			while(i-- >= 0) {
				cheatChars[i+1] = cheatChars[i];
			}
				
			cheatChars[0] = e.keyCode;
			
			if(	 	cheatChars[0] == Keyboard.N
				&& 	cheatChars[1] == Keyboard.O
				&& 	cheatChars[2] == Keyboard.S
				&& 	cheatChars[3] == Keyboard.L
				&& 	cheatChars[4] == Keyboard.I
				&& 	cheatChars[5] == Keyboard.W 
				&& 	cheatChars[6] == Keyboard.R 
				&& 	cheatChars[7] == Keyboard.R 
				&& 	cheatChars[8] == Keyboard.A 
				&& 	cheatChars[9] == Keyboard.M 
				&& 	cheatChars[10] == Keyboard.A 
				&& 	cheatChars[11] == Keyboard.L  ) {
					
					cassettes = 999;
					playSound("Assets/Powerup52.wav");
					togglePause();
			
			}
			
			if(	 	cheatChars[0] == Keyboard.E
				&& 	cheatChars[1] == Keyboard.R
				&& 	cheatChars[2] == Keyboard.A 
				&& 	cheatChars[3] == Keyboard.D 
				&& 	cheatChars[4] == Keyboard.M 
				&& 	cheatChars[5] == Keyboard.U 
				&& 	cheatChars[6] == Keyboard.D 
				&& 	cheatChars[7] == Keyboard.U 
				&& 	cheatChars[8] == Keyboard.L  ) {
					
					cassettes = 999;
					playSound("Assets/Powerup52.wav");
					togglePause();
			
			}
			
			if(	 	cheatChars[0] == Keyboard.Y
				&& 	cheatChars[1] == Keyboard.A
				&& 	cheatChars[2] == Keyboard.M 
				&& 	cheatChars[3] == Keyboard.S 
				&& 	cheatChars[4] == Keyboard.E 
				&& 	cheatChars[5] == Keyboard.M 
				&& 	cheatChars[6] == Keyboard.A 
				&& 	cheatChars[7] == Keyboard.J  ) {
					
					lastCheckPoint = 15;
					lvl = 14;			
					nextLevel();
					togglePause();
			
			}
			
			
		}
		
		
	}
	
	private function on_keyUp(e:KeyboardEvent):Void {
		
		if(e.keyCode == 32) {
			if(running && !paused) {
					
				hud.buttons[4].toggle();
			
			}
		}
		
	}
	
	public function buttonCallback(btn:UIBtn) {
	
		if(!running || paused) return;
		
		playSound("Assets/Pickup_Coin56.wav");
		
		if(btn.id != 4 && !btn.state) btn.toggle();
		
		if(btn.id == 0) {
			//reverse
			if(btn.state && hud.buttons[1].state) 
				hud.buttons[1].toggle();
				
			if(btn.state && hud.buttons[5].state) 
				hud.buttons[5].toggle();
		}
		
		if(btn.id == 1) {
			//throttle
			if(btn.state && hud.buttons[0].state) 
				hud.buttons[0].toggle();
				
			if(btn.state && hud.buttons[5].state) 
				hud.buttons[5].toggle();
		}
		
		if(btn.id == 2) {
			//left
			if(btn.state && hud.buttons[3].state) 
				hud.buttons[3].toggle();
		}
		
		if(btn.id == 3) {
			//right
			if(btn.state && hud.buttons[2].state) 
				hud.buttons[2].toggle();
		}
		
		if(btn.id == 4) {
			//stop
			for(btn in hud.buttons) 
				if(btn.state) btn.toggle();
			
			if(lvl < 5)
				hud.indicate();
			
		}
	
		if(btn.id == 5) {
			//play
			if(btn.state && hud.buttons[0].state) 
				hud.buttons[0].toggle();
				
			if(btn.state && hud.buttons[1].state) 
				hud.buttons[1].toggle();
				
		}
		
	
		
	}
	
	public function playSound(path:String):Void {
	
		if(!owner.sound) return;
		
		var soundfx = new Sound();
		soundfx = Assets.getSound(path);
		soundfx.play();
	
	}
	
	private function restart(e:MouseEvent):Void {
			
		if(ticks - wait < 10) return; //do nothing
		
		owner.playMusic("Assets/music2.mp3");
		
		Lib.current.stage.removeEventListener(MouseEvent.CLICK, restart);

		cover.hide();
		
		lvl = lastCheckPoint;
		cassettes = 5;
		running = true;	
		
		reset();
		
		playSound("Assets/Pickup_Coin80.wav");
		
	}
	
	private function asPoint(c:Coord):Point {
		return new Point((c.x + .5) * 32, (c.y + .5) * 32);
	}
	
	private function asCoord(p:Point):Coord {
		return new Coord(Math.floor(p.x / 32), Math.floor(p.y / 32));
	}
	
	private function togglePause() {
		
		if(!running) return;
		
		paused = !paused;
		pauseMenu.visible = !pauseMenu.visible;
		if(paused)
			owner.playMusic("Assets/music3.mp3");
		else 
			owner.playMusic("Assets/music2.mp3");
	}
	
	private function toggleSound(e:MouseEvent) {
	
		playSound("Assets/Pickup_Coin56.wav");
		
		
	
		owner.sound = !owner.sound;
		
		soundbtn.bitmapData = owner.sound ? SpriteSheet.loadSprite("SOUND_ON") : SpriteSheet.loadSprite("SOUND_OFF");
		
		if(!owner.sound) {
			
			owner.stopMusic();
		
		} else {

			owner.resumeMusic();
		
		}
	
	}
	
	private function explode(x:Float, y:Float):Void {
	
		var explosion:Sprite = new Sprite();
		
		explosion.addChild(new Bitmap(SpriteSheet.loadSprite("EXPLOSION")));
		
		explosion.x = x-16;
		explosion.y = y-16;
		
		explosion.alpha = 1;
		
		world.addChild(explosion);
		
		functionFadeout = fadeout(explosion);
		explosion.addEventListener(Event.ENTER_FRAME, functionFadeout);
	
	}
	
	private function fadeout(target:Sprite) {
		
		return function (e:Event):Void {
		
			if(target.alpha > 0) {
				
				target.alpha -= 1/60;
			}	
			
			else {
				target.removeEventListener(Event.ENTER_FRAME, functionFadeout);
				explosionCallback();
				target.parent.removeChild(target);
			}
		
		}
				
	}
	
	private function mark(x:Float, y:Float):Void {
		
		var marker:Bitmap = new Bitmap(new BitmapData(2, 2, 0xff0000));
		
		marker.x = x;
		marker.y = y;
		
		world.addChild(marker);
		
	}
	
	private function exit(e:MouseEvent):Void {
		System.exit(0);
	}

}
