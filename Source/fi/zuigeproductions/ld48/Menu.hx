/*

Menu.hx

Main Menu

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import flash.Lib;

import flash.display.Sprite;
import flash.display.BitmapData;
import flash.display.Bitmap;

import flash.events.Event;
import flash.events.MouseEvent;

import flash.ui.Mouse;

import fi.zuigeproductions.ld48.Main;
import fi.zuigeproductions.ld48.Game;
import fi.zuigeproductions.ld48.SpriteSheet;

class Menu extends Sprite {

	public var owner:Game;
	
	private var yesButton:StupidButton;
	private var noButton:StupidButton;
	
	private var frames:Int = 0;
	private var wait:Int = -1;
	private var startButton:Sprite;
	private var graphic:Bitmap;
	private var logo:Bitmap;
	
	public function new(?owner:Game = null) {
	
		super();
		
		if(owner != null)
			this.owner = owner;
			
		buildMenu();	
		
		#if !flash
		fullscreenDialogue();
		#else
		showSplash();
		#end
		
	}		
	
	private function buildMenu():Void {
	
		logo = new Bitmap();
		addChild(logo);
		
		startButton = new Sprite();
		graphic = new Bitmap();
		
		startButton.addChild(graphic);
		graphic.bitmapData = SpriteSheet.loadSprite("START GAME");
		graphic.x = -graphic.width / 2;
		
		addChild(startButton);
		startButton.x = Main.GAME_WIDTH / 2;
		startButton.y = Main.GAME_HEIGHT - 30;
		startButton.visible = false;
		
	}

	private function fullscreenDialogue():Void {
	
		logo.bitmapData = SpriteSheet.loadSprite("FULLSCREEN");
		
		yesButton = new StupidButton("YES");
		noButton = new StupidButton("NO");
		
		yesButton.y = noButton.y = 160;
		yesButton.x = 155;
		noButton.x = 155 + 3*32;
		
		yesButton.addEventListener(MouseEvent.ROLL_OVER, rollOver);
		noButton.addEventListener(MouseEvent.ROLL_OVER, rollOver);
		
		yesButton.addEventListener(MouseEvent.ROLL_OUT, rollOut);
		noButton.addEventListener(MouseEvent.ROLL_OUT, rollOut);
		
		yesButton.addEventListener(MouseEvent.CLICK, yes);
		noButton.addEventListener(MouseEvent.CLICK, no);
		
		addChild(yesButton);
		addChild(noButton);
		
		//addbuttons
		
	}
	
	private function rollOver(e:MouseEvent):Void {
		e.target.bitmap.bitmapData = SpriteSheet.loadSprite(e.target.sprite+"_2");
		#if flash
		Mouse.cursor = "button";		
		#end
	}
	
	private function rollOut(e:MouseEvent):Void {
		e.target.bitmap.bitmapData = SpriteSheet.loadSprite(e.target.sprite);
		#if flash
		Mouse.cursor = "arrot";		
		#end
	}
	
	private function yes(e:MouseEvent):Void {
		removeChild(yesButton);
		removeChild(noButton);
		Main.fullscreen();
		owner.fullscreen = true;
		showSplash();
	}
	
	private function no(e:MouseEvent):Void {
		removeChild(yesButton);
		removeChild(noButton);
		showSplash();
	}
	
	private function showSplash():Void {
	
		// TODO: build menu
		
		wait = frames;
		
		logo.bitmapData = SpriteSheet.loadSprite("SPLASHSCREEN");
		
		Lib.current.stage.addEventListener(MouseEvent.CLICK, startGame);
		
		startButton.visible = true;
		
		this.addEventListener(Event.ENTER_FRAME, animate);
			
		
	}
	
	private function startGame(e:MouseEvent):Void {
	
		if(wait == frames) return;
	
		owner.startGame();
		
		Lib.current.stage.removeEventListener(MouseEvent.CLICK, startGame);
		
	}
	
	private function animate(e:Event):Void {
		
		++frames;
		
		if(frames % 5 == 0) {
		
			graphic.bitmapData = (frames % 10 == 0) ? SpriteSheet.loadSprite("START GAME") : SpriteSheet.loadSprite("START GAME_2");
		
		}
		
	}

}

class StupidButton extends Sprite {

	public var bitmap:Bitmap;
	public var sprite:String;
	
	public function new(sprite:String) {
	
		super();
		
		this.sprite = sprite;
		this.bitmap = new Bitmap(SpriteSheet.loadSprite(sprite));
		this.addChild(this.bitmap);
	
	}
	

}
