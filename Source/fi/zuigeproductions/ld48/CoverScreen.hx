/*

CoverScreen.hx

Description

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import flash.display.Sprite;
import flash.display.Bitmap;

import flash.events.Event;

import fi.zuigeproductions.ld48.SpriteSheet;

class CoverScreen extends Sprite {
	
	public var screen:String;
	
	private var graphic:Bitmap;
	private var flasher:Bitmap;
	
	private var animate:Bool = false;
	private var ticks:Int = 0;
	
	private var proceed:String;
	
	public function new () {
		
		super();
		
		this.graphic = new Bitmap();
		this.flasher = new Bitmap();
		
		this.flasher.bitmapData = SpriteSheet.loadSprite(proceed);
		
		flasher.x = Main.GAME_WIDTH / 2 - flasher.width / 2;
		flasher.y = Main.GAME_HEIGHT - 30;
		
		addChild(graphic);
		addChild(flasher);
		
		this.addEventListener(Event.ENTER_FRAME, on_enterFrame);
		
	}
	
	public function fadeIn():Void {
		this.visible = true;
		animate = true;
	}		
	
	public function hide():Void {
	
		this.visible = false;
		this.alpha = 0;
	
	}
	
	public function show():Void {
	
		this.visible = true;
		this.alpha = 1;
		
	}
	
	public function setGraphic(sprite:String, proceed:String) {
		
		this.screen = sprite;
		this.graphic.bitmapData = SpriteSheet.loadSprite(sprite);
		
		this.proceed = proceed;
		
		flasher.bitmapData = SpriteSheet.loadSprite(proceed);
		flasher.x = Main.GAME_WIDTH / 2 - flasher.width / 2;
		flasher.y = Main.GAME_HEIGHT - 30;
	
	}
	
	private function on_enterFrame(e:Event):Void {
	
		if(animate) {
			if(this.alpha < 1) {
				this.alpha += 1 / 24;
			}
			else {
				this.animate = false;
				this.alpha = 1;
			}
		}
		
		if(this.visible) {
		
			ticks++;
			flasher.bitmapData = (Math.floor(ticks / 5) % 2 == 0) ? SpriteSheet.loadSprite(proceed) : SpriteSheet.loadSprite(proceed+"_2");
		
		}
	
	}

}
