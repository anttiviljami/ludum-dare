/*

Overlay.hx

Description

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import flash.display.Sprite;
import flash.display.Bitmap;
import flash.display.BitmapData;

import flash.text.TextField;
import flash.text.TextFormat;
import flash.text.Font;
import flash.text.AntiAliasType;

import flash.events.Event;
import flash.events.MouseEvent;

import flash.ui.Mouse;

import openfl.Assets;

import fi.zuigeproductions.ld48.SpriteSheet;
import fi.zuigeproductions.ld48.Game;

import openfl.display.FPS;

class Overlay extends Sprite {

	public var lvldisplay:TextField;
	public var lvlshadow:TextField;
	
	public var attemptdisplay:TextField;
	public var attemptshadow:TextField;
	
	public var counter:TextField;
	public var owner:Game;
	
	public var helper:Bitmap;
	
	public var buttons:Array<UIBtn>;
	
	private var indicator:Int = 0;
	private var ticks:Int = 0;
	
	public function new (owner) {
		
		super();
		
		this.owner = owner;
		
		buildHUD();
		
	}		
	
	public function update():Void {
		
		counter.text = "Cycles: " + owner.robot.cycles + "ms"; 
		lvldisplay.text = "Level " + owner.lvl; 
		attemptdisplay.text = "Cassettes: " + owner.cassettes; 
		
	}
	
	private function buildHUD ():Void {
		
		var timeline:Bitmap = new Bitmap();
		timeline.bitmapData = new BitmapData(Main.GAME_WIDTH, 32, 0xff7c838f);
		
		var i = 0;
		while(i < Main.GAME_WIDTH) 
			timeline.bitmapData.setPixel(i++, 0, 0xffffffff);

		timeline.y = Main.GAME_HEIGHT - 32;
		this.addChild(timeline);
		
		buttons = new Array();
		
		i = 0;
		while(i < 6) {
			
			var btn = new UIBtn(owner);
			btn.id = i;
			
			btn.graphic.bitmapData = SpriteSheet.loadSprite("BTN" + i);
			
			btn.y = Main.GAME_HEIGHT - 32 - 10;
			btn.x = 20 + 36 * i;
			
			this.addChild(btn);
			buttons.push(btn);
			
			++i;
		}	
		
		counter = new TextField(); 
		counter.embedFonts = true;
		counter.antiAliasType = AntiAliasType.NORMAL;
		counter.defaultTextFormat = new TextFormat("Assets/lowres", 32, 0xffffffff);
		counter.width = 180;
		counter.selectable = false;
		counter.x = Main.GAME_WIDTH - counter.width;
		counter.y = Main.GAME_HEIGHT - 26;
		
		counter.text = "Cycles: 10000ms";
		
		this.addChild(counter);
		
		lvldisplay = new TextField(); 
		lvldisplay.embedFonts = true;
		lvldisplay.antiAliasType = AntiAliasType.NORMAL;
		lvldisplay.defaultTextFormat = new TextFormat("Assets/lowres", 32, 0xffffffff);
		lvldisplay.width = 180;
		lvldisplay.selectable = false;
		lvldisplay.x = 3;
		lvldisplay.y = 3;
		
		lvldisplay.text = "Level 0";
		
		attemptdisplay = new TextField(); 
		attemptdisplay.embedFonts = true;
		attemptdisplay.antiAliasType = AntiAliasType.NORMAL;
		attemptdisplay.defaultTextFormat = new TextFormat("Assets/lowres", 16, 0xffffffff);
		attemptdisplay.width = 180;
		attemptdisplay.selectable = false;
		attemptdisplay.x = 3;
		attemptdisplay.y = 26;
		
		attemptdisplay.text = "Attempt 0";
		
		
		helper = new Bitmap();
		helper.bitmapData = SpriteSheet.loadSprite("HELPER");
		helper.x = 100;
		helper.y = Main.GAME_HEIGHT - 70;
		
		helper.visible = false;
		
		
		this.addChild(lvldisplay);
		this.addChild(attemptdisplay);
		this.addChild(helper);
		
		this.addEventListener(Event.ENTER_FRAME, animate);
		
		//this.addChild(new FPS());
		
	}
	
	public function indicate():Void {

		indicator = 200;
	
	}
	
	private function animate(e:Event) {
	
		ticks++;
		
		if(indicator > 0) {
			helper.visible = Math.floor(indicator / 30) % 2 == 0;
			indicator--;
		}
		
		else helper.visible = false;
	
	}
	

}

class UIBtn extends Sprite {
	
	public var owner:Game;
	public var id:Int;
	public var graphic:Bitmap;
	
	public var state:Bool = false;
	
	public function new (owner) {
		
		super();
		
		this.owner = owner;
		this.addEventListener(MouseEvent.CLICK, callback);
		this.addEventListener(MouseEvent.MOUSE_DOWN, on_mouseDown);
		this.addEventListener(MouseEvent.MOUSE_UP, on_mouseUp);
		this.addEventListener(MouseEvent.MOUSE_OVER, on_mouseOver);
		this.addEventListener(MouseEvent.MOUSE_OUT, on_mouseOut);
		
		this.graphic = new Bitmap();
		this.addChild(graphic);
		
	}
	
	public function toggle():Void {
		this.state = !this.state;
		this.graphic.bitmapData = SpriteSheet.loadSprite("BTN" + this.id + (this.state ? "_DOWN" : ""));
		this.y = this.state ? Main.GAME_HEIGHT - 32 - 6 : Main.GAME_HEIGHT - 32 - 10;
	}
	
	private function callback(e:MouseEvent):Void {	
		owner.buttonCallback(this);
	}
	
	private function on_mouseDown(e:MouseEvent):Void {
		this.graphic.bitmapData = SpriteSheet.loadSprite("BTN" + this.id + "_DOWN");
		this.y = Main.GAME_HEIGHT - 32 - 6;
	}
	
	private function on_mouseUp(e:MouseEvent):Void {
		this.graphic.bitmapData = SpriteSheet.loadSprite("BTN" + this.id + (this.state ? "_DOWN" : ""));
		this.y = this.state ? Main.GAME_HEIGHT - 32 - 6 : Main.GAME_HEIGHT - 32 - 10;
	}
	
	private function on_mouseOver(e:MouseEvent):Void {
		#if flash
		Mouse.cursor = "button";		
		#end
		this.graphic.bitmapData = SpriteSheet.loadSprite("BTN" + this.id + (this.state ? "_DOWN" : ""));
		this.y = this.state ? Main.GAME_HEIGHT - 32 - 6 : Main.GAME_HEIGHT - 32 - 10;
	}
	private function on_mouseOut(e:MouseEvent):Void {
		#if flash
		Mouse.cursor = "arrow";		
		#end
		this.graphic.bitmapData = SpriteSheet.loadSprite("BTN" + this.id + (this.state ? "_DOWN" : ""));
		this.y = this.state ? Main.GAME_HEIGHT - 32 - 6 : Main.GAME_HEIGHT - 32 - 10;
	}
	
}