/*

SpriteSheet.hx

Description

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/
package fi.zuigeproductions.ld48;

import openfl.Assets;

import flash.display.Bitmap;
import flash.display.BitmapData;

import flash.geom.Rectangle;
import flash.geom.Matrix;

class SpriteSheet {

	private static var PATH:String = "Assets/spritesheet.png";

	private var source:BitmapData;
	
	public static function loadSprite(name:String):BitmapData {
	
		var rect:Rectangle = spriteRect(name);
		
		var sprite:BitmapData = new BitmapData(Math.floor(rect.width), Math.floor(rect.height), true, 0);
		
		sprite.draw(
			Assets.getBitmapData(PATH), 
			new Matrix(1, 0, 0, 1, -rect.x, -rect.y), 
			null, 
			null, 
			new Rectangle(0, 0, rect.width, rect.height)
		);
		
		return sprite;
	
	}

	private static function spriteRect(name:String):Rectangle {
		
		return switch(name) {
		
			case "GIMP" : new Rectangle(0, 0, 32, 32);
			case "GIT" : new Rectangle(32, 0, 32, 32);
			case "CASSETTE" : new Rectangle(64, 0, 32, 32);
			case "TROPHY" : new Rectangle(128, 0, 32, 32);
			
			case "WALL0" : new Rectangle(0, 32, 32, 32);
			case "WALL1" : new Rectangle(32, 32, 32, 32);
			case "WALL2" : new Rectangle(64, 32, 32, 32);
			
			case "LASER_SRC" : new Rectangle(128, 32, 32, 32);
			case "LASER" : new Rectangle(160, 32, 32, 32);
			
			case "EXPLOSION" : new Rectangle(96, 0, 32, 32);
			
			case "BTN0" : new Rectangle(0, 64, 32, 32);
			case "BTN1" : new Rectangle(32, 64, 32, 32);
			case "BTN2" : new Rectangle(64, 64, 32, 32);
			case "BTN3" : new Rectangle(96, 64, 32, 32);
			case "BTN4" : new Rectangle(128, 64, 32, 32);
			case "BTN5" : new Rectangle(160, 64, 32, 32);
			
			case "BTN0_DOWN" : new Rectangle(0, 96, 32, 32);
			case "BTN1_DOWN" : new Rectangle(32, 96, 32, 32);
			case "BTN2_DOWN" : new Rectangle(64, 96, 32, 32);
			case "BTN3_DOWN" : new Rectangle(96, 96, 32, 32);
			case "BTN4_DOWN" : new Rectangle(128, 96, 32, 32);
			case "BTN5_DOWN" : new Rectangle(160, 96, 32, 32);
			
			case "SPLASHSCREEN" : new Rectangle(0, 288, 480, 270);
			case "PROLOGUE" : new Rectangle(0, 576, 480, 270);
			case "FULLSCREEN" : new Rectangle(0, 864, 480, 270);
			case "ENDSCREEN" : new Rectangle(0, 1152, 480, 270);
			
			case "PAUSEMENU" : new Rectangle(0, 1440, 480, 270);
			
			case "START GAME" : new Rectangle(0, 128, 128, 32); 
			case "START GAME_2" : new Rectangle(0, 160, 128, 32); 
			
			case "REPLAY" : new Rectangle(0, 192, 128, 32); 
			case "REPLAY_2" : new Rectangle(0, 224, 128, 32);
			
			case "HELPER" : new Rectangle(320, 192, 160, 32);
			
			case "YES" : new Rectangle(320, 224, 64, 32); 
			case "YES_2" : new Rectangle(320, 256, 64, 32); 
			
			case "NO" : new Rectangle(416, 224, 64, 32); 
			case "NO_2" : new Rectangle(416, 256, 64, 32); 
			
			case "EXIT" : new Rectangle(256, 96, 32, 32);
			
			case "SOUND_ON" :  new Rectangle(256, 64, 32, 32); 
			case "SOUND_OFF" :  new Rectangle(288, 64, 32, 32); 
			
			default: new Rectangle(0, 32, 32, 32);
		
		}
	
	}

}