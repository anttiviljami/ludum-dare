/*

git.hx

Description

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import flash.display.Sprite;
import flash.display.Bitmap;
import flash.display.BitmapData;

import fi.zuigeproductions.ld48.SpriteSheet;

class Git extends Sprite {
	
	public function new () {
	
		super();
		
		var bitmap = new Bitmap(SpriteSheet.loadSprite("GIT"));
		bitmap.x = -bitmap.width / 2;
		bitmap.y = -bitmap.height / 2;
		
		addChild(bitmap);
		
	}		

}
