/*

Map.hx

Description

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import flash.Lib;

import flash.display.Sprite;
import flash.display.BitmapData;
import flash.display.Bitmap;
import flash.geom.Matrix;
import flash.geom.Rectangle;
import flash.geom.Point;

import openfl.Assets;

import fi.zuigeproductions.ld48.SpriteSheet;

class Map extends Sprite {
	
	public var spawn:Coord;
	public var git:Coord;
	
	public var gameObjects:Array<GameObject>;
	public var animated:Array<GameObject>;
	
	private static var CHUNK_WIDTH:Int = 8;
	private static var CHUNK_HEIGHT:Int = 8;
	
	private var source:BitmapData;
	
	private var parseX:Int;
	private var parseY:Int;
	
	private var map:Array<Array<Block>>;
	private var chunks:Array<Bitmap>;
	
	private var variant:Int = 0;
	
	public function new (path:String) {
		
		super();		

		this.source = Assets.getBitmapData(path);
		
		parseX = parseY = 0;
		
		map = new Array();
		chunks = new Array();
		gameObjects = new Array();
		animated = new Array();
		
		parseBitmap();
		
	}
	
	public function getBlock(c:Coord):Block {
		
		if(c.x < 0 || c.x >= source.width || c.y < 0 || c.y >= source.height) {
			//outside of level
			return new Block(WALL, "WALL");
		}
		
		return map[c.x][c.y];
		
	}
	
	private function parseBitmap():Void {
	
		while(parseY < source.height) {
		
			if(parseX >= source.width) {
				parseX = 0;
				parseY++;
			}
			
			if(parseY == 0) {
				map[parseX] = new Array();
			}
			
			var type = blockType(source.getPixel(parseX, parseY));
			var sprite = blockSprite(source.getPixel(parseX, parseY));
			
			//store blocks
			map[parseX][parseY] = new Block(type, sprite);
			
			if(type == SPAWN)
				spawn = new Coord(parseX, parseY);
				
			if(type == GIT)
				git = new Coord(parseX, parseY);
				
			if(type == LASER) {
				
				var step:Int = 0;
				
				while(blockType(source.getPixel(parseX, parseY-step)) != WALL) {
					map[parseX][parseY - step] = new Block(LASER, "LASER");
					animated.push(new GameObject(LASER, new Coord(parseX, parseY - step)));
					step++;
				}	
				
				gameObjects.push(new GameObject(LASER, new Coord(parseX, parseY)));
			}
			
			parseX++;
		
		}
	
	}
	
	private function renderChunk(coord:Coord) {
	
		//top-left corner of chunk
		var chunkPos:Coord = new Coord(
			Math.floor(coord.x / CHUNK_WIDTH) * CHUNK_WIDTH,  
			Math.floor(coord.y / CHUNK_HEIGHT) * CHUNK_HEIGHT);
		
		var dummyChunk:Sprite = new Sprite(); //temporary drawing container for chunk
		var dummy:Bitmap;
		
		var block:Block;
		var sprite:BitmapData;
	
		for(y in 0...CHUNK_HEIGHT) {
			for(x in 0...CHUNK_WIDTH) {
				
				block = getBlock(new Coord(chunkPos.x + x, chunkPos.y + y));
				
				if(block.sprite == "NONE") continue; //don't render empty sprites
				
				if(block.sprite == "LASER") continue; //leave laser rendering
				
				else if(block.sprite == "WALL")
					sprite = SpriteSheet.loadSprite(block.sprite + "" + variant);
				else 
					sprite = SpriteSheet.loadSprite(block.sprite);

				dummy = new Bitmap();
				dummy.bitmapData = sprite;
				dummy.x = x*32;
				dummy.y = y*32;

				//place block
				dummyChunk.addChild(dummy);
				
			}
		
		}
		
		var chunkdata:BitmapData = new BitmapData(
			CHUNK_WIDTH*32, 
			CHUNK_HEIGHT*32, 
			true, 
			0x00000000
		);
		
		chunkdata.draw(dummyChunk); //draw to actual chunk
		dummyChunk = null; //remove dummy
		
		var chunk:Bitmap = new Bitmap(chunkdata);
		
		//smoothing
		//chunk.smoothing = Tuba.BITMAP_SMOOTHING;
		//chunk.pixelSnapping = PixelSnapping.ALWAYS;
		
		//position chunk
		chunk.x = chunkPos.x * 32;
		chunk.y = chunkPos.y * 32;
		
		//display and store chunk
		addChild(chunk);
		chunks.push(chunk);
	
	}
	
	public function renderRect(bounds:Rectangle, variant:Int):Void {
		
		
		this.variant = variant;
		//remove chunks out of rect
		var i:Int = 0;
		while (i < chunks.length) {
			if(!chunks[i].getBounds(this).intersects(bounds)) {
				removeChild(chunks[i]);
				chunks[i] = null;
				chunks.splice(i, 1);
				continue;
			}
			i++;
		}
		
		var x:Int;
		var y:Int;
		
		y = 0;
		while ((y-CHUNK_HEIGHT)*32 <= bounds.height) {
			x = 0;
		
			while ((x-CHUNK_WIDTH)*32 <= bounds.width) {	
		
				var pos:Coord = new Coord(Math.floor(bounds.x / 32) + x, Math.floor(bounds.y / 32) + y);
				
				if (x*32 > bounds.width) 
					pos.x = Math.floor((bounds.x + bounds.width) / 32);
					
				if (y*32 > bounds.height)
					pos.y = Math.floor((bounds.y + bounds.height) / 32); 
				
				if(!isRendered(pos)) {
					renderChunk(pos);
				}
				
				x += CHUNK_WIDTH;
			}
			
			y += CHUNK_HEIGHT;
		}
		
		//chunksRendered = chunks.length;
		
	}	
	
	private function blockType(pixel:Int):BlockType {
	
		return switch (pixel) {
		
			case 0xffff00 : SPAWN;
			case 0x6666ff : WALL;
			case 0x00ff00 : GIT;
			
			case 0xff00ff : LASER;
			
			default : EMPTY; 
		
		}
	
	}
	
	private function blockSprite(pixel:Int):String {
	
		return switch (pixel) {
		
			case 0x6666ff : "WALL";
			
			default : "NONE";
					
		}
	
	}
	
	//checks if the chunk containing the coord is already rendered
	private function isRendered(coord:Coord):Bool {
		
		for (c in chunks) {
			if(c.getBounds(this).containsPoint(asPoint(coord))) 
				return true;
		}
		
		return false;
	}
	
	//conversions between Point and Coord 	
	public function asPoint(c:Coord):Point {
		return new Point((c.x + .5) * 32, (c.y + .5) * 32);
	}
	public function asCoord(p:Point):Coord {
		return new Coord(Math.floor(p.x / 32), Math.floor(p.y / 32));
	}

}

enum BlockType {

	SPAWN;
	WALL;
	GIT;
	
	LASER;
	
	EMPTY;

}

class GameObject {

	public var type:BlockType;
	public var pos:Coord;
	
	public function new(type, pos) {
	
		this.type = type;
		this.pos = pos;
	
	}

}

class Block {

	public var type:BlockType;
	public var sprite:String;
	
	public function new (type:BlockType, ?sprite:String = "NONE"):Void {
	
		this.type = type;
		this.sprite = sprite;
		
	}

}

class Coord {

	public var x:Int;
	public var y:Int;
	
	public function new (x:Int, y:Int):Void {
		this.x = x;
		this.y = y;
	}

}