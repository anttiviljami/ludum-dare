/*

Main.hx

App launcher / controller

Antti Kuosmanen <antti@zuigeproductions.fi>

Copyright (c) Zuige Productions 2013

*/

package fi.zuigeproductions.ld48;

import fi.zuigeproductions.ld48.Game;
import fi.zuigeproductions.ld48.Menu;

import flash.Lib;
import flash.display.Sprite;
import flash.display.StageQuality;
import flash.display.StageAlign;
import flash.display.StageScaleMode;
import flash.display.StageDisplayState;

import flash.media.Sound;
import flash.media.SoundChannel;
import flash.media.SoundTransform;

import flash.events.Event;

import openfl.Assets;

class Main {

	public static var GAME_WIDTH:Int = 480;
	public static var GAME_HEIGHT:Int = 270;
	
	public static var FULLSCREEN:Bool = false;
	
	public static var GAME_FPS:Int = 60;
	
	public var scale:Float = 2;

	public var game:Game;
	
	private var music:SoundChannel;
	private var piece:Sound;
	
	public var menu:Menu;

	private var frame:Sprite;
	
	public var sound:Bool = true;
	
	public function new () {
	
		//sets up fullscreen
		if(FULLSCREEN) {
			Lib.current.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
		}
		
		#if flash
		music = new SoundChannel();
		#else
		music = new SoundChannel("asd", 0, 0xff, new SoundTransform());
		#end
		
		playMusic("Assets/music.mp3");
	
		frame = new Sprite();
		
		game = new Game(this);
			
		Lib.current.stage.addChild(game);
		game.scaleX = game.scaleY = scale;	
		
		menu = new Menu(game);
		game.addChild(menu);
		
		Lib.current.stage.addEventListener(Event.RESIZE, onResize);
		resize();
		Lib.current.stage.addChild(frame);
	
	}
	
	public static function fullscreen():Void {
		Lib.current.stage.displayState = StageDisplayState.FULL_SCREEN_INTERACTIVE;
	}
	
	private function onResize(e:Event):Void {
		resize();
	}
	
	private function resize() {
	
		if( Lib.current.stage.stageWidth / Lib.current.stage.stageHeight < GAME_WIDTH / GAME_HEIGHT) {
			game.scaleX = game.scaleY = Math.floor(Lib.current.stage.stageWidth/GAME_WIDTH);
		} 
		
		else {
			game.scaleX = game.scaleY = Math.floor(Lib.current.stage.stageHeight/GAME_HEIGHT);
		}
		
		game.x = Lib.current.stage.stageWidth/2 - GAME_WIDTH*game.scaleX / 2;
		game.y = Lib.current.stage.stageHeight/2 - GAME_HEIGHT*game.scaleX / 2;
	
		frame.graphics.clear();
		frame.graphics.beginFill(0x000000);
		frame.graphics.drawRect(0, 0, Lib.current.stage.stageWidth, game.y); 
		frame.graphics.drawRect(0, game.y + GAME_HEIGHT * game.scaleX,  Lib.current.stage.stageWidth, Lib.current.stage.stageHeight);
		frame.graphics.drawRect(0, game.y, game.x, GAME_HEIGHT * game.scaleX);
		frame.graphics.drawRect(game.x + GAME_WIDTH * game.scaleX, game.y,  game.x, GAME_HEIGHT * game.scaleX);
		frame.graphics.endFill();		
		
	}
	
	public function playMusic(path:String) {
	
		if(!sound) return;
	 	stopMusic();
	 	piece = Assets.getSound(path);
	 	music = piece.play(0, 0xff);
	}
	
	public function stopMusic():Void {
		music.stop();
		music = null;
	}
	
	public function resumeMusic():Void {
		music = piece.play();
	}
	
}
